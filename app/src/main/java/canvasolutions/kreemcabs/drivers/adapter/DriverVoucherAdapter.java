package canvasolutions.kreemcabs.drivers.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import canvasolutions.kreemcabs.drivers.R;
import canvasolutions.kreemcabs.drivers.model.VoucherModel;

public class DriverVoucherAdapter extends RecyclerView.Adapter<DriverVoucherAdapter.MyVoucherViewHolder>{

    Activity activity;
    ArrayList<VoucherModel> voucherModelArrayList;
    LayoutInflater inflater;

    public DriverVoucherAdapter(Activity activity,
                                  ArrayList<VoucherModel> voucherModelArrayList) {
        this.activity = activity;
        this.voucherModelArrayList = voucherModelArrayList;
        this.inflater = activity.getLayoutInflater();
    }

    @NonNull
    @Override
    public MyVoucherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_applied_vouchers, parent, false);
        return new DriverVoucherAdapter.MyVoucherViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyVoucherViewHolder holder, int position) {
        final VoucherModel voucherModel = voucherModelArrayList.get(position);
        holder.txtPromoCodeName.setText(voucherModel.getTxtPromoCodeName());
        holder.txtPromoCodeStatus.setText(voucherModel.getTxtPromoCodeStatus());
    }

    @Override
    public int getItemCount() {
        return voucherModelArrayList.size();
    }


    public static class MyVoucherViewHolder extends RecyclerView.ViewHolder{
        private final TextView txtPromoCodeName;
        private final TextView txtStartDate;
        private final TextView txtEndDate;
        private final TextView txtPromoCodeStatus;

        public MyVoucherViewHolder(View view) {
            super(view);
            txtPromoCodeName = view.findViewById(R.id.txtPromoCodeName);
            txtStartDate = view.findViewById(R.id.txtStartDate);
            txtEndDate = view.findViewById(R.id.txtEndDate);
            txtPromoCodeStatus = view.findViewById(R.id.txtPromoCodeStatus);
        }

    }

}
