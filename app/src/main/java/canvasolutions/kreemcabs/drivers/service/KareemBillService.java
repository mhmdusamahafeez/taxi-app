package canvasolutions.kreemcabs.drivers.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcelable;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import canvasolutions.kreemcabs.drivers.R;
import canvasolutions.kreemcabs.drivers.activity.MainActivity;

import static canvasolutions.kreemcabs.drivers.settings.AppConst.LOCATION_BROADCAST;

public class KareemBillService extends Service {
    public static final String TAG = "KareemBillService";
    private static final int NOTIFICATION_ID = 100;
    private static final String NOTIFICATION_CHANNEL_ID = "location_channel_id";
    int waitingTime = 0;
    boolean isWaitingTimerStarted = false;
    CountDownTimer countDownTimer;
    double distance = 0;
    boolean hasSpeed = false;
    float speed = 0;
    BillObject billObject;

    long lastTimeLocationReceived = 0;
    private NotificationManager notificationManager;
    private LocationRequest locationRequest;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationCallback locationCallback;
    private Location currentLocation;

    private final IBinder localBinder = new MyBinder();

    @Override
    public void onCreate() {
        super.onCreate();
        billObject = new BillObject();
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = new LocationRequest()
                .setInterval(TimeUnit.SECONDS.toMillis(1))
                .setFastestInterval(TimeUnit.SECONDS.toMillis(1))
                .setMaxWaitTime(TimeUnit.SECONDS.toMillis(1))
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if (currentLocation != null) {
                    if (locationResult.getLastLocation() != null) {
                        double distanceTravelled = currentLocation.distanceTo(locationResult.getLastLocation());   //distance in meters
                        if(distanceTravelled > 8) {
                            distance += distanceTravelled;
                        }
                        if(locationResult.getLastLocation().hasSpeed()) {
                            hasSpeed = true;
                            speed = locationResult.getLastLocation().getSpeed();
                        } else{
                            hasSpeed = false;
                        }
                        calculateWaitingTime();

                        Intent intent = new Intent(LOCATION_BROADCAST);
                        intent.putExtra("BillObject", getBillObject());
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                        Log.d(TAG, "Distance: " + distance);
                        Log.d(TAG, "Speed: " + speed);
                        Log.d(TAG, "Longitude: " + locationResult.getLastLocation().getLongitude() + " - " +
                                "Latitude: " + locationResult.getLastLocation().getLatitude());
                    }
                }
                currentLocation = locationResult.getLastLocation();
                lastTimeLocationReceived = System.currentTimeMillis();

                notificationManager.notify(NOTIFICATION_ID, generateNotification(currentLocation));
            }
        };
    }

    private float getSpeed(double distanceTravelled) {
        long timeDiff = System.currentTimeMillis() - lastTimeLocationReceived;
        return (float) (distanceTravelled / timeDiff);
    }

    private void calculateWaitingTime() {
        if (hasSpeed && MPerSToKmPerH(speed) < 5) {
            if (!isWaitingTimerStarted) {
                isWaitingTimerStarted = true;
                Log.d(TAG, "CountDownTimer Started!");
                countDownTimer = new CountDownTimer(5 * 1000, 1000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        if (MPerSToKmPerH(speed) < 5) {
                            waitingTime += 5;
                            isWaitingTimerStarted = false;
                            Log.d(TAG, "CountDownTimer Ended!");
                        }
                    }
                }.start();
            }
        } else {
            if(countDownTimer != null) {
                isWaitingTimerStarted = false;
                countDownTimer.cancel();
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
            startForeground(NOTIFICATION_ID, generateNotification(currentLocation));
        } else {
            Log.d(TAG,"Location Permission Missing!");
        }


        return START_STICKY;
    }

    public BillObject getBillObject() {
        BillObject billObject = new BillObject();
        billObject.distance = distance;
        billObject.longitude = currentLocation.getLongitude();
        billObject.latitude = currentLocation.getLatitude();
        billObject.hasSpeed = hasSpeed;
        billObject.speed = speed;
        billObject.waitingTime = waitingTime;
        return billObject;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }


    public class MyBinder extends Binder {
        public KareemBillService getService() {
            return KareemBillService.this;
        }
    }

    public Notification generateNotification(Location location) {
        String mainNotificationText = "";
        if (location != null) {
            //mainNotificationText ="k-kareem Service";
            mainNotificationText += String.format(Locale.getDefault(),"WaitingTime: %d\nDistance: %.2fM - Speed: %.2fKM/H",
                    waitingTime,
                    (distance),
                    (MPerSToKmPerH(speed)));

        } else {
            mainNotificationText += "No Current Location!";
        }
        String titleText = "K-Kareem Driver - Location";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, titleText, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle().setBigContentTitle(titleText).bigText(mainNotificationText);
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        NotificationCompat.Builder notificationCompatBuilder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID);
        return notificationCompatBuilder
                .setStyle(bigTextStyle)
                .setContentTitle(titleText)
                .setContentText(mainNotificationText)
                .setSmallIcon(R.drawable.logo)
                .setContentIntent(pendingIntent)
                .setSound(null)
                .setSilent(true)
                .setNotificationSilent()
                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                .setOngoing(true)
                .build();
    }

    public float MPerSToKmPerH(float speed) {
        return speed * 3.6f;
    }
}
