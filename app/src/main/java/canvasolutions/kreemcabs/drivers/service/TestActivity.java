package canvasolutions.kreemcabs.drivers.service;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import canvasolutions.kreemcabs.drivers.R;
import canvasolutions.kreemcabs.drivers.controller.AppController;
import canvasolutions.kreemcabs.drivers.model.M;
import canvasolutions.kreemcabs.drivers.settings.AppConst;

import static canvasolutions.kreemcabs.drivers.settings.AppConst.LOCATION_BROADCAST;

public class TestActivity extends AppCompatActivity {
    KareemBillService kareemBillService;
    boolean mBound = false;

    TextView tv_longitude;
    TextView tv_latitude;
    TextView tv_distance;
    TextView tv_has_speed;
    TextView tv_speed;
    TextView tv_waitingTime;
    TextView tv_wallet_price;
    TextView tv_bill_info;
    Button btn_stop_service;

    BroadcastReceiver broadcastReceiver;
    LocationManager locationManager;


    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            KareemBillService.MyBinder binder = (KareemBillService.MyBinder) iBinder;
            kareemBillService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        broadcastReceiver = new LocationBroadcastReceiver();
        registerReceiver();
        tv_longitude = findViewById(R.id.tv_longitude);
        tv_latitude = findViewById(R.id.tv_latitude);
        tv_distance = findViewById(R.id.tv_distance);
        tv_has_speed = findViewById(R.id.tv_has_speed);
        tv_speed = findViewById(R.id.tv_speed);
        tv_waitingTime = findViewById(R.id.tv_waiting_time);
        tv_wallet_price = findViewById(R.id.tv_wallet_price);
        tv_bill_info = findViewById(R.id.tv_bill_info);
        btn_stop_service = findViewById(R.id.btn_stop_service);

        btn_stop_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mBound) {
                    calculateBill();
                    unbindService(connection);
                    stopService(new Intent(TestActivity.this,KareemBillService.class));
                    mBound = false;
                }
            }
        });
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            ContextCompat.startForegroundService(this, new Intent(this, KareemBillService.class));
            Toast.makeText(this, "GPS enabled", Toast.LENGTH_SHORT).show();
        } else{
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
        getBillValues();

    }

    private float perKmTravellingRate, startRate, minimum_cost;
    private float perSecondWaitingTimeRate;

    public void getBillValues() {
        String url = AppConst.Server_url + "return_vehicle_type.php?id="+M.getID(TestActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    try {
                        JSONObject json = new JSONObject(response);
                        Log.d("TestActivity",response);
                        float wait_cost_per_h = Float.parseFloat(json.getString("wait_cost/h"));
                        float mini_cost = Float.parseFloat(json.getString("minimum_cost"));
                        float starting_cost = Float.parseFloat(json.getString("starting_cost"));

                        perKmTravellingRate = Float.parseFloat(json.getString("prix"));
                        startRate = starting_cost;
                        minimum_cost = mini_cost;
                        perSecondWaitingTimeRate = wait_cost_per_h/3600;

                        Toast.makeText(this,"Values Received",Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            Toast.makeText(this, "Failed getting values!", Toast.LENGTH_SHORT).show();
        });
        AppController.getInstance().addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    public void calculateBill(){
        BillObject kareemBillObject = kareemBillService.getBillObject();
        float waitingAmount = kareemBillObject.waitingTime * perSecondWaitingTimeRate;
        float travellingAmount = (float) (kareemBillObject.getDistanceInKM() * perKmTravellingRate);

        double totalBill = waitingAmount + travellingAmount + startRate;
        if(totalBill < minimum_cost){
            totalBill = minimum_cost;
        }
        tv_bill_info.setText(String.format(Locale.getDefault(),
                "Waiting Amount: %.2f\nTravelling Amount: %.2f\nStart Rate: %.2f\nTotal Bill: %.2f",
                waitingAmount,travellingAmount,startRate,totalBill));
        tv_bill_info.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, KareemBillService.class);
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            bindService(intent, connection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(connection);
        mBound = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterReceiver();
    }

    public void registerReceiver(){
        IntentFilter intentFilter = new IntentFilter(LOCATION_BROADCAST);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,intentFilter);
        Toast.makeText(this, "Receiver Registered", Toast.LENGTH_SHORT).show();
    }

    public void unRegisterReceiver(){
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        Toast.makeText(this, "Receiver Unregistered", Toast.LENGTH_SHORT).show();
    }

    private class LocationBroadcastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            if(LOCATION_BROADCAST.equals(intent.getAction())) {
                BillObject billObject = (BillObject) intent.getSerializableExtra("BillObject");
                tv_longitude.setText("Longitude: " + billObject.longitude);
                tv_latitude.setText("Latitude: " + billObject.latitude);
                tv_distance.setText(String.format(Locale.getDefault(),"Distance: %.2fm",billObject.distance));
                tv_has_speed.setText(billObject.hasSpeed ? "Has Speed: YES" : "Has Speed: NO");
                tv_speed.setText(String.format(Locale.getDefault(),"Speed: %.2fm/s",billObject.speed));
                tv_speed.setText(String.format(Locale.getDefault(),"Speed: %.2fkm/h",billObject.getSpeedInKMPerH()));
                tv_waitingTime.setText("Waiting Time: " + billObject.waitingTime+"s");
            }
        }
    }
}
