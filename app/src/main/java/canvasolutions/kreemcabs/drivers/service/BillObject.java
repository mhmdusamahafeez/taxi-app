package canvasolutions.kreemcabs.drivers.service;

import java.io.Serializable;

public class BillObject implements Serializable {
    public double longitude, latitude;
    public double distance;
    public boolean hasSpeed;
    public float speed;
    public int waitingTime;

    /////////////////
    public float perKmTravellingRate, startRate, minimumCost, perSecondWaitingTimeRate;
    public float travellingAmount, waitingAmount;
    public double totalBill;

    //////////////

    public float getSpeedInKMPerH() {
        return speed * 3.6f;
    }

    public double getDistanceInKM(){
        return distance/1000;
    }
}
