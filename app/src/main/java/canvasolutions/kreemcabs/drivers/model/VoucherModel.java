package canvasolutions.kreemcabs.drivers.model;

public class VoucherModel {
    //private int id;
    private String txtPromoCodeName;
    private String txtStartDate;
    private String txtEndDate;
    private String txtPromoCodeStatus;

    public VoucherModel(String txtPromoCodeName, String txtStartDate, String txtEndDate,
                        String txtPromoCodeStatus) {
        this.txtPromoCodeName = txtPromoCodeName;
        this.txtStartDate = txtStartDate;
        this.txtEndDate = txtEndDate;
        this.txtPromoCodeStatus = txtPromoCodeStatus;
    }

    public String getTxtPromoCodeName() {
        return txtPromoCodeName;
    }

    public void setTxtPromoCodeName(String txtPromoCodeName) {
        this.txtPromoCodeName = txtPromoCodeName;
    }

    public String getTxtStartDate() {
        return txtStartDate;
    }

    public void setTxtStartDate(String txtStartDate) {
        this.txtStartDate = txtStartDate;
    }

    public String getTxtEndDate() {
        return txtEndDate;
    }

    public void setTxtEndDate(String txtEndDate) {
        this.txtEndDate = txtEndDate;
    }

    public String getTxtPromoCodeStatus() {
        return txtPromoCodeStatus;
    }

    public void setTxtPromoCodeStatus(String txtPromoCodeStatus) {
        this.txtPromoCodeStatus = txtPromoCodeStatus;
    }
}
