package canvasolutions.kreemcabs.drivers.fragment.driver;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import canvasolutions.kreemcabs.drivers.R;
import canvasolutions.kreemcabs.drivers.adapter.DriverVoucherAdapter;
import canvasolutions.kreemcabs.drivers.controller.AppController;
import canvasolutions.kreemcabs.drivers.model.M;
import canvasolutions.kreemcabs.drivers.model.VoucherModel;
import canvasolutions.kreemcabs.drivers.settings.AppConst;

public class VoucherDiscountFragment extends Fragment {

    AppCompatButton btnAddVoucher;
    EditText edt_txt_Voucher;
    ProgressBar pbCode;
    ProgressBar pbAppliedVouchers;
    TextView txtNoCodeApplied;
    SwipeRefreshLayout swipe_refresh_voucher;

    RecyclerView rvVoucher;
    public static ArrayList<VoucherModel> modelArrayList;
    public static DriverVoucherAdapter voucherAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_voucher_discount, container, false);
        getPromoCodeValues(M.getID(getContext()), "conductor");

        btnAddVoucher = view.findViewById(R.id.btnAddVoucher);
        edt_txt_Voucher = view.findViewById(R.id.edt_txt_Voucher);
        pbCode = view.findViewById(R.id.pbCode);
        pbAppliedVouchers = view.findViewById(R.id.pbAppliedVouchers);
        txtNoCodeApplied = view.findViewById(R.id.txtNoCodeApplied);
        rvVoucher = view.findViewById(R.id.rvVoucher);
        swipe_refresh_voucher = view.findViewById(R.id.swipe_refresh_voucher);

        swipe_refresh_voucher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPromoCodeValues(M.getID(getContext()), "conductor");
            }
        });

        btnAddVoucher.setOnClickListener(v -> {
            String promoCode = edt_txt_Voucher.getText().toString();
            if(promoCode.isEmpty()){
                Toast.makeText(getContext(), "Enter Code", Toast.LENGTH_SHORT).show();
            }else {
                pbCode.setVisibility(View.VISIBLE);
                postVoucher(promoCode);
            }
        });

        return  view;
    }

    public void postVoucher(String code){
        String url = AppConst.Server_url+"set_voucher.php";
        StringRequest jsonObjectReq = new StringRequest(Request.Method.POST, url, response -> {
           // M.hideLoadingDialog();
            pbCode.setVisibility(View.GONE);
            try {
                JSONObject json = new JSONObject(response);
                String responseMessage = json.getString("response");

                if(responseMessage.equals("used")){
                    new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Alert")
                            .setContentText("Sorry You have already used this promo code!!")
                            .show();
                    edt_txt_Voucher.setText("");
                    getPromoCodeValues(M.getID(getContext()), "conductor");


                }else if(responseMessage.equals("ok")){
                    new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Congratulation")
                            .setContentText("Promo amount added to wallet!!")
                            .show();
                    edt_txt_Voucher.setText("");

                }else {
                    new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Alert")
                            .setContentText("Invalid promo code!!")
                            .show();
                    edt_txt_Voucher.setText("");

                }

                //Toast.makeText(getContext(), "Response Message = "+responseMessage, Toast.LENGTH_SHORT).show();


            } catch (JSONException e){
                e.printStackTrace();
            }
        }, error -> M.hideLoadingDialog()){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", String.valueOf(M.getID(getContext())));
                params.put("promo_code", code );
                params.put("type", "conductor");

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjectReq);
        jsonObjectReq.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    public void getPromoCodeValues(String getRideUserIDs, String type) {
        String url = AppConst.Server_url + "get_voucher_all.php?user_id=" + getRideUserIDs +
                "&type=" + type;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, response -> {
            modelArrayList = new ArrayList<>();
            try {
                Log.d("VoucherFragment", response);
                if(swipe_refresh_voucher.isRefreshing())
                    swipe_refresh_voucher.setRefreshing(false);
                //txtNoCodeApplied.setVisibility(View.GONE);


                JSONObject json = new JSONObject(response);
                String voucher = json.getString("value");
                for (int i = 0; i < voucher.length(); i++) {
                    modelArrayList.clear();
                    modelArrayList.add(new VoucherModel(
                            json.getString("promo"),
                            json.getString("value"),
                            json.getString("user_type"),
                            json.getString("status")

                    ));

                    if(modelArrayList == null && modelArrayList.size()==0){
                        txtNoCodeApplied.setVisibility(View.VISIBLE);
                        pbAppliedVouchers.setVisibility(View.VISIBLE);
                    }

                    txtNoCodeApplied.setVisibility(View.GONE);
                    pbAppliedVouchers.setVisibility(View.GONE);
                    voucherAdapter = new DriverVoucherAdapter(getActivity(), modelArrayList);
                    LinearLayoutManager verticalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                    rvVoucher.setLayoutManager(verticalLayoutManager);
                    rvVoucher.setAdapter(voucherAdapter);


                }


                /*String voucher = json.getString("value");
                promoStatus = json.getString("status");*/

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> {
            txtNoCodeApplied.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), "Failed getting values!", Toast.LENGTH_SHORT).show();
        });
        AppController.getInstance().addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

}