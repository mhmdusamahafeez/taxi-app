package canvasolutions.kreemcabs.drivers.fragment.driver;



import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import canvasolutions.kreemcabs.drivers.R;
import canvasolutions.kreemcabs.drivers.activity.MainActivity;
import canvasolutions.kreemcabs.drivers.adapter.RideDriverAdapter;
import canvasolutions.kreemcabs.drivers.controller.AppController;
import canvasolutions.kreemcabs.drivers.model.M;
import canvasolutions.kreemcabs.drivers.model.RideModel;
import canvasolutions.kreemcabs.drivers.service.BillObject;
import canvasolutions.kreemcabs.drivers.service.KareemBillService;
import canvasolutions.kreemcabs.drivers.service.TestActivity;
import canvasolutions.kreemcabs.drivers.settings.AppConst;
import canvasolutions.kreemcabs.drivers.settings.ConnectionDetector;

import static canvasolutions.kreemcabs.drivers.settings.AppConst.LOCATION_BROADCAST;

public class FragmentRideOnRideDriver extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener,
        GoogleMap.OnMyLocationClickListener,
        GoogleMap.OnMyLocationButtonClickListener,
        RideDriverAdapter.RideDriverListener {

        BroadcastReceiver broadcastReceiver;

    ViewPager pager;
    TabLayout tabs;
    View view;
    public static Context context;
    public static ConnectionDetector connectionDetector;
    String TAG = "FragmentHome";
    ArrayList<String> tabNames = new ArrayList<String>();
    int currpos = 0;

    public static RecyclerView recycler_view_ride;
    public static List<RideModel> albumList_ride;
    public static RideDriverAdapter adapter_ride;

    /**
     * MAP
     **/
    private GoogleMap mMap;
    public static Location currentLocation, clientLocation, destinationLocation;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private static final int LOCATION_REQUEST_CODE = 101;

    /**
     * GOOGLE API CLIENT
     **/
    private GoogleApiClient googleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private LocationRequest locationRequest;
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000; // = 5 seconds

    View mapView;
    private LocationManager locationManager;
    private final int REQUEST_FINE_LOCATION = 1234;
    private String provider;
    private int PLACE_PICKER_REQUEST = 1;

    public static SwipeRefreshLayout swipe_refresh;

    public static ProgressBar progressBar_failed;
    public static LinearLayout layout_not_found, layout_failed;
    public static RelativeLayout layout_liste;
    private static LinearLayout linear_layout;
    private static RideModel rideModel = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null)
            currpos = getArguments().getInt("tab_pos", 0);

        broadcastReceiver = new LocationBroadcastReceiver();
        registerReceiver();
        getBillValues();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ride_on_ride, container, false);

        context = getActivity();
        MainActivity.setTitle(getString(R.string.arrived));
//        if(M.getCountry(context).equals("All"))
//            MainActivity.setTitle("On ride");
//        else
//            MainActivity.setTitle("On ride - "+M.getCountry(context));
        connectionDetector = new ConnectionDetector(context);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
//            return;
        }

        // Get the location manager
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the locatioin provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        if (provider != null) {
            currentLocation = locationManager.getLastKnownLocation(provider);
            clientLocation = locationManager.getLastKnownLocation(provider);
            destinationLocation = locationManager.getLastKnownLocation(provider);
        }

        // we build google api client
        googleApiClient = new GoogleApiClient.Builder(context)
//                .enableAutoManage(getActivity(),0,this)
                .addApi(LocationServices.API)
//                .addApi(Places.GEO_DATA_API)
//                .addApi(Places.PLACE_DETECTION_API)
//                .enableAutoManage(getActivity(), this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();

        albumList_ride = new ArrayList<>();
        adapter_ride = new RideDriverAdapter(context, albumList_ride, getActivity(), "RideOnRide");
        adapter_ride.setRideDriverListener(this);

        linear_layout = (LinearLayout) view.findViewById(R.id.linear_layout);
        progressBar_failed = (ProgressBar) view.findViewById(R.id.progressBar_failed);
        layout_liste = (RelativeLayout) view.findViewById(R.id.layout_liste);
        layout_not_found = (LinearLayout) view.findViewById(R.id.layout_not_found);
        layout_failed = (LinearLayout) view.findViewById(R.id.layout_failed);
        swipe_refresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        recycler_view_ride = (RecyclerView) view.findViewById(R.id.recycler_view_requetes);
        @SuppressLint("WrongConstant") LinearLayoutManager verticalLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recycler_view_ride.setLayoutManager(verticalLayoutManager);
        recycler_view_ride.setItemAnimator(new DefaultItemAnimator());
        recycler_view_ride.setAdapter(adapter_ride);

        swipe_refresh.setOnRefreshListener(() -> new getRideOnRide().execute());

        layout_failed.setOnClickListener(view -> {
            progressBar_failed.setVisibility(View.VISIBLE);
            new getRideOnRide().execute();
        });

        swipe_refresh.setRefreshing(true);
        new getRideOnRide().execute();

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
    }

    private static void showSnackbar(final int position) {
        Snackbar snackbar = Snackbar
                .make(linear_layout, "You accepted the order successfully.", Snackbar.LENGTH_LONG);
        snackbar.setAction("UNDO", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                adapter_ride.restoreItem(rideModel, position);
                recycler_view_ride.scrollToPosition(position);
            }
        });

        snackbar.setActionTextColor(Color.YELLOW);
        snackbar.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        this.currentLocation = location;
//        Toast.makeText(context, "Ok", Toast.LENGTH_SHORT).show();
        if (location != null) {
//            getDistance(location);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // Permissions ok, we get last location
        currentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        startLocationUpdates();
    }

    private void startLocationUpdates() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(this, "You need to enable permissions to display location !", Toast.LENGTH_SHORT).show();
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
    }

    public static class getRideOnRide extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String url = AppConst.Server_url + "get_requete_on_ride.php";
            StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                    url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            try {
                                swipe_refresh.setRefreshing(false);
                                albumList_ride.clear();
                                adapter_ride.notifyDataSetChanged();
                                JSONObject json = new JSONObject(response);
                                JSONObject msg = json.getJSONObject("msg");
                                String etat = msg.getString("etat");
                                if (etat.equals("1")) {
                                    layout_liste.setVisibility(View.VISIBLE);
                                    layout_not_found.setVisibility(View.GONE);
                                    layout_failed.setVisibility(View.GONE);
                                    progressBar_failed.setVisibility(View.GONE);

                                    String distance_client = "";
                                    for (int i = 0; i < (msg.length() - 1); i++) {
                                        JSONObject taxi = msg.getJSONObject(String.valueOf(i));
                                        albumList_ride.add(new RideModel(taxi.getInt("id"), taxi.getInt("id_user_app"), taxi.getInt("id_conducteur"), taxi.getString("nom") + " " + taxi.getString("prenom"), taxi.getString("nomConducteur") + " " + taxi.getString("prenomConducteur"), taxi.getString("distance"), taxi.getString("creer"), taxi.getString("statut"), taxi.getString("latitude_depart")
                                                , taxi.getString("longitude_depart"), taxi.getString("latitude_arrivee"), taxi.getString("longitude_arrivee")
                                                , taxi.getString("niveau"), taxi.getString("moyenne"), taxi.getString("nb_avis"), taxi.getString("montant"), taxi.getString("duree"), taxi.getString("depart_name")
                                                , taxi.getString("destination_name"), taxi.getString("trajet"), taxi.getString("driverPhone"), taxi.getString("phone"), taxi.getString("statut_paiement")
                                                , taxi.getString("payment"), taxi.getString("payment_image"), taxi.getString("place"), taxi.getString("number_poeple"), taxi.getString("heure_retour"), taxi.getString("statut_round"), taxi.getString("date_retour"), taxi.getString("photo_path"), taxi.getString("comment")));
                                        adapter_ride.notifyDataSetChanged();
                                    }
                                } else {
                                    layout_liste.setVisibility(View.GONE);
                                    layout_not_found.setVisibility(View.VISIBLE);
                                    layout_failed.setVisibility(View.GONE);
                                    progressBar_failed.setVisibility(View.GONE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, error -> {
                        layout_liste.setVisibility(View.GONE);
                        layout_not_found.setVisibility(View.GONE);
                        layout_failed.setVisibility(View.VISIBLE);
                        progressBar_failed.setVisibility(View.GONE);
                    }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("id_driver", M.getID(context));
                    return params;
                }

            };
            AppController.getInstance().addToRequestQueue(jsonObjReq);
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            //to add spacing between cards
            if (this != null) {

            }

        }

        @Override
        protected void onPreExecute() {

        }
    }

    public static void showNotFound() {
        layout_liste.setVisibility(View.GONE);
        layout_not_found.setVisibility(View.VISIBLE);
        layout_failed.setVisibility(View.GONE);
        progressBar_failed.setVisibility(View.GONE);
    }


    ////////////////////////////////////////////////////////////////////////////////
    private float perKmTravellingRate, startRate, minimum_cost;
    private float perSecondWaitingTimeRate;

    public void getBillValues() {
        String url = AppConst.Server_url + "return_vehicle_type.php?id="+M.getID(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    try {
                        JSONObject json = new JSONObject(response);
                        Log.d("TestActivity",response);
                        float wait_cost_per_h = Float.parseFloat(json.getString("wait_cost/h"));
                        float mini_cost = Float.parseFloat(json.getString("minimum_cost"));
                        float starting_cost = Float.parseFloat(json.getString("starting_cost"));

                        perKmTravellingRate = Float.parseFloat(json.getString("prix"));
                        startRate = starting_cost;
                        minimum_cost = mini_cost;
                        perSecondWaitingTimeRate = wait_cost_per_h/3600;

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            Toast.makeText(getContext(), "Failed getting values!", Toast.LENGTH_SHORT).show();
        });
        AppController.getInstance().addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    KareemBillService kareemBillService;
    BillObject kareemBillObject;
    boolean mBound = false;

    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            KareemBillService.MyBinder binder = (KareemBillService.MyBinder) iBinder;
            kareemBillService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBound = false;
        }
    };

    /** Start COOGLE API Client**/
    @Override
    public void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
        requireContext().bindService(new Intent(requireContext(),KareemBillService.class),mServiceConnection,Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
        //requireContext().unbindService(mServiceConnection);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(broadcastReceiver != null) {
            unRegisterReceiver();
        }
    }

    public void registerReceiver(){
        IntentFilter intentFilter = new IntentFilter(LOCATION_BROADCAST);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver,intentFilter);
        //Toast.makeText(getContext(), "Receiver Registered", Toast.LENGTH_SHORT).show();
    }

    public void unRegisterReceiver(){
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
    }

    private class LocationBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(LOCATION_BROADCAST.equals(intent.getAction())) {
                BillObject billObject = (BillObject) intent.getSerializableExtra("BillObject");

            }
        }
    }

    @Override
    public void onStartRideClick() {
        ContextCompat.startForegroundService(context, new Intent(context, KareemBillService.class));
        if(!mBound)
            requireContext().bindService(new Intent(getContext(),KareemBillService.class),mServiceConnection,Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStopRideClick() {
        BillObject billObject = kareemBillService.getBillObject();
        float waitingAmount = billObject.waitingTime * perSecondWaitingTimeRate;
        float travellingAmount = (float) (billObject.getDistanceInKM() * perKmTravellingRate);

        double subTotalBill = waitingAmount + travellingAmount + startRate;
        double totalBill = waitingAmount + travellingAmount + startRate;
        if(totalBill < minimum_cost){
            totalBill = minimum_cost;
        }

        billObject.perKmTravellingRate = perKmTravellingRate;
        billObject.perSecondWaitingTimeRate = perSecondWaitingTimeRate;
        billObject.startRate = startRate;
        billObject.minimumCost = minimum_cost;
        billObject.travellingAmount = travellingAmount;
        billObject.waitingAmount = waitingAmount;
        billObject.totalBill = totalBill;
        this.kareemBillObject = billObject;

        Log.d(TAG, "Waiting Amount: " + waitingAmount);
        Log.d(TAG, "Travelling Amount: " + travellingAmount);
        Log.d(TAG, "Start Rate: " + startRate);
        Log.d(TAG,"Total Bill: " + totalBill);
       // Toast.makeText(getContext(), "Bill to be payed: "+totalBill, Toast.LENGTH_SHORT).show();

        requireContext().unbindService(mServiceConnection);
        requireContext().stopService(new Intent(context,KareemBillService.class));

     /*   if(mBound) {

            requireContext().unbindService(mServiceConnection);
            requireContext().stopService(new Intent(context,KareemBillService.class));
            mBound = false;
        }*/

        adapter_ride.billForDriver(totalBill,travellingAmount,startRate,waitingAmount,subTotalBill,minimum_cost);
        requireContext().stopService(new Intent(context, KareemBillService.class));

    }

    @Override
    public void onCompleteRide(RideModel rideModel) {
        adapter_ride.generateBillClient(rideModel,kareemBillObject);

    }
}
