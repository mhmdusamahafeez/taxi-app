package canvasolutions.kreemcabs.drivers.fragment.driver;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import canvasolutions.kreemcabs.drivers.R;

public class ContactUsFragment extends Fragment {

    LinearLayout txtWhatsApp;
    LinearLayout btnPhone;
    TextView txtEmail;
    TextView makePhoneCall;
    TextView txtWebAddress;
    private static final String CONTACT_SUPPORT = "0483210413";
    private static final String WHATSAPP_SUPPORT = "+923135527658";
    private static final String CONTACT_EMAIL = "Info@k-kareemcabs.com";
    private static final int REQUEST_PHONE_CALL = 1;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_contact_us, container, false);

        txtWhatsApp = view.findViewById(R.id.txtWhatsApp);
        btnPhone = view.findViewById(R.id.btnPhone);
        txtEmail = view.findViewById(R.id.txtEmail);
        makePhoneCall = view.findViewById(R.id.makePhoneCall);
        txtWebAddress = view.findViewById(R.id.txtWebAddress);

        txtWhatsApp.setOnClickListener(v -> {
            String url = "https://api.whatsapp.com/send?phone=" + WHATSAPP_SUPPORT;
            Intent contactSupport = new Intent(Intent.ACTION_VIEW);
            contactSupport.setData(Uri.parse(url));
            startActivity(contactSupport);
        });

        btnPhone.setOnClickListener(v -> callNumber(CONTACT_SUPPORT));

        makePhoneCall.setOnClickListener(v -> callNumber(makePhoneCall.getText().toString()));

        txtEmail.setOnClickListener(v -> composeEmail(CONTACT_EMAIL, ""));

        txtWebAddress.setOnClickListener(v -> {
            String url = "https://www.k-kareemcabs.com/";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        });
        return  view;
    }

    public void composeEmail(String addresses, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void callNumber(String number){
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
        }
        else
        {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel: "+number.trim()));
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            getContext().startActivity(callIntent);
        }
    }
}